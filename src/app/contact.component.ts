import {Component,Input } from '@angular/core';
import {Contact} from './contact.service';
@Component({
	selector:'contact',
	templateUrl:'./contact.component.html',
	styles:[`
	
	`]
})

export class ContactComponent{
	@Input() contact:Contact;
}