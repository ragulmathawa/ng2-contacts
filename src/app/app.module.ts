import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ContactService} from './contact.service';
import { AppComponent } from './app.component';
import {ContactComponent} from './contact.component';
import {ContactPaneComponent} from './contactPane.component';
import {ContactDetailComponent} from './contactDetail.component';
import {RouterModule,Routes} from '@angular/router';

const appRoutes: Routes = [
  { path: 'contact/:id', component: ContactDetailComponent },
  {path: '',component:ContactDetailComponent}
];
@NgModule({
  declarations: [
    AppComponent,ContactPaneComponent,ContactComponent,ContactDetailComponent
  ],
  imports: [
    NgbModule.forRoot(),
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],  
  bootstrap: [AppComponent]
})
export class AppModule { }
