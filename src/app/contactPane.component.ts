import {Component} from '@angular/core';
import {ContactService,Contact} from './contact.service';
import {Router} from '@angular/router';
@Component({
	selector: 'contactPane',
	templateUrl:'./contactPane.component.html',
	styleUrls:['./contactPane.component.css'],
	providers:[ContactService]	
})

export class ContactPaneComponent{
	contacts:Contact[];
	active:Contact=null;
	newContact:Contact=new Contact();
	hasError:boolean=false;
	newContactExpanded:boolean=false;
	constructor(private contactService:ContactService,private router:Router){
	}
	onNewContact(){
		console.log("Add new Contact...");
		if(this.checkFields()){
			//add new Contact
			let tcontact:Contact=new Contact();
			tcontact.name=this.newContact.name;
			tcontact.mobile=this.newContact.mobile;
			this.contactService.addContact(tcontact).then(res=>
						{ 
							console.log("contact added."); 
							this.newContact=new Contact();
							this.hasError=false;
							this.newContactExpanded=false;
							this.router.navigate(['contact',tcontact.id]);
							this.contactService.setStatusMessage("New contact created.");
						})
					.catch(res=>
					{
						this.contactService.setStatusMessage("Cannot create Contact. Error!!");
					});
					
		}else{
			console.log(this.newContact);
			console.log("Error in adding new Contact");
			this.contactService.setStatusMessage("Please provide a name.");
			this.hasError=true;
		}
	}
	onShowNewContact(){
		this.newContactExpanded=true;
	}
	onCancelNewContact(){
		console.log("Cancel new Contact");
		this.hasError=false;
		this.newContactExpanded=false;
		this.newContact=new Contact();
	}
	private checkFields(){
		if(this.newContact.name && this.newContact.name.length!=0)
			return true;
		return false;
	}
	onContactSelect(contact:Contact){
		this.active=contact;
	}
	ngOnInit():void{
		this.contactService.getContacts().then(contacts=> this.contacts=contacts);
	}
}