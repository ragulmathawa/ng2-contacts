import {Component,Input } from '@angular/core';
import {Contact} from './contact.service';
import {ActivatedRoute,Router} from '@angular/router';
import {ContactService} from './contact.service';
@Component({
	selector:'contact-detail',
	templateUrl:'./contactDetail.component.html',
	styleUrls:['./contactDetail.component.css']
})

export class ContactDetailComponent{
	contact:Contact;
	tcontact:Contact;
	constructor(private route:ActivatedRoute,private contactService:ContactService,private router:Router){
	}
	ngOnInit(){
		this.route.params.subscribe(params=>this.contactService.getContact(params['id'])
						.then(contact=>
							{
								this.contact=contact;
								this.tcontact=JSON.parse(JSON.stringify(this.contact));
							})
						.catch(res=>console.log("no contact")));
	}
	isInputsSame(){
		return this.contact.name==this.tcontact.name&&this.contact.mobile==this.tcontact.mobile;
	}
	update(){
	this.contactService.updateContact(this.tcontact)
						.then(res=>
							{	
								console.log("Updated");
								this.contactService.setStatusMessage("Contact Updated.");
							})
						.catch(res=>
							{
								console.log("update failed");
								this.contactService.setStatusMessage("Cannot update. Error!!")
							});
	}
	reset(){
		this.tcontact=JSON.parse(JSON.stringify(this.contact));
	}
	delete(){
		this.contactService.deleteContact(this.tcontact.id)
						.then(res=>
							{
								console.log("deleted");
								this.contactService.setStatusMessage("Contact Deleted."); 
								this.router.navigate(['/'])
							})
						.catch(res=>
							{
								console.log("deletion failed");
								this.contactService.setStatusMessage("Cannot Delete. Error!!");
							});
	}
}