import {Injectable } from '@angular/core';

@Injectable()

export class Contact{
	name:string;
	mobile:string;
	id:number;
}
export var contacts :Contact[]=[
];

export class ContactService{
	private idSequence:number;
	public statusMsg:String;
	getContacts():Promise<Contact[]>{
		return Promise.resolve(contacts);
	}

	public setStatusMessage(msg){
		this.statusMsg=msg;
	}

	getNextId():number{
		this.idSequence=Number(localStorage.getItem("contactsng2Sequence"));
		localStorage.setItem('contactsng2Sequence',""+(this.idSequence+1));
		return this.idSequence++;

	}
	addContact(contact:Contact):Promise<Object>{
		contact.id=this.getNextId();
		contacts.push(contact);
		return Promise.resolve({"success":true});
	}
	getContact(id:number){
		return Promise.resolve(contacts.filter(tid=>id==tid.id).pop());
	}

	constructor(){
		let json=localStorage.getItem("contactsng2");
		this.idSequence=Number(localStorage.getItem("contactsng2Sequence"));
		if(json&&json.length>0){
			contacts=JSON.parse(json);
		}
		console.log(json);
	}
	updateContact(contact:Contact){
		let obj=contacts.filter(c=>contact.id==c.id).pop();
		let ind=contacts.findIndex(c=>c.id==contact.id);
		if(ind>-1){
			contacts[ind]=JSON.parse(JSON.stringify(contact));
			localStorage.setItem("contactsng2",JSON.stringify(contacts));
		return Promise.resolve({"success":true});	
		}else
		return Promise.reject({'success':false});
		
	}

	deleteContact(id){
		let ind=contacts.findIndex(c=>c.id==id);
		if(ind>-1){
			contacts.splice(ind,1);
			localStorage.setItem("contactsng2",JSON.stringify(contacts));
			return Promise.resolve({"success":true});
		}else
		return Promise.reject({'success':false});
	}
}