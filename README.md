# Contacts

    This is a simple contact manager developed using Angular 2. The contacts are stored in browser local storage not in the server.
    
    * You can Create new contact
    * Edit the contact
    * Delete the Contact

## Dependencies

* Node Js 6.X
* angular-cli 1.0.0-beta.24

## Installing Angular-cli

* Through Node package Manager `sudo npm install -g angular-cli`

## Setup

1. Clone the Git repository `git clone https://gitlab.com/ragulmathawa/ng2-contacts.git`
2. move to local repo `cd ng2-contacts`
3. Install dependencies `npm install`
4. Run Local Server `ng serve`
4. The App is available in [http://localhost:4200](http://localhost:4200)